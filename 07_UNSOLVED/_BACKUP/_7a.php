<?php
(isset($argv[1]) && is_readable($argv[1])) || die("error reading file\n");
(isset($argv[2]) && preg_match('/^[a-z]+$/', $argv[2])) || die("wrong wire specification\n");

$ins = file($argv[1], FILE_IGNORE_NEW_LINES);
$tw = $argv[2];
// 1st step
function find($c) {
	global $ins;
	foreach($ins as $in) {
		if (preg_match('/-> ' . $c . '$/', $in)) {
			return '(' . substr($in, 0, strpos($in, ' ->')) . ')';
		}
	}
	return "";
}
function sub($str, $lvl = 0) {
	$m = [];
	echo $lvl . str_repeat(".", $lvl) . "$str\n";
	$str = preg_replace_callback(
		'/[a-z]+/', 
		function($m) {
			return find($m[0]);
		},
		$str
	);
	if (preg_match('/[a-z]+/', $str)) {
		$r = sub($str, $lvl + 1);
		return $r;
	} else {
		return $str;
	}

}
$i = 0;
$str = find($tw);
while(preg_match('/[a-z]+/', $str)) {
	$str = preg_replace_callback(
		'/[a-z]+/',
		function($m) {
			return find($m[0]);
		},
		$str
	);
	echo $i++ . str_repeat('.', $i) . " $str\n";
}

// echo sub(find($tw)) . "\n";
