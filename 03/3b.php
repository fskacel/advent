<?php
$x = 0;
$y = 0;
$xx = 0; $yy = 0;

(isset($argv[1]) && is_readable($argv[1])) || die('error opening file');

$file = file_get_contents($argv[1]);

$map = ['0|0'];
for ($i = 0; $i < strlen($file) - 1; $i++) {
	if ($i % 2) {
		switch($file[$i]) {
			case '^': $y++; break;
			case 'v': $y--; break;
			case '>': $x++; break;
			case '<': $x--; break;
			default: die('invalid input');
		}
		$coords = "$x|$y";
	} else {
		switch($file[$i]) {
			case '^': $yy++; break;
			case 'v': $yy--; break;
			case '>': $xx++; break;
			case '<': $xx--; break;
			default: die('invalid input');
		}
		$coords = "$xx|$yy";
	}

	echo $coords;

	if (!in_array($coords, $map, TRUE)) {
		$map[] = $coords;
		echo ' *';
	}

	echo "\n";

}

echo 'visited houses:' .  count($map) . "\n\n";
