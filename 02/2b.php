<?php
(isset($argv[1]) && is_readable($argv[1])) || die ("ERROR opening input file\n\n");
$f = file($argv[1]);
$sum = 0;
$r_sum = 0;
foreach ($f as $l) {
	$m = [];
	preg_match('/(\d+)x(\d+)x(\d+)/', $l, $m);
	array_splice($m, 0, 1);
	$surface = 2 * ($m[0] * $m[1]) + 2 * ($m[1] * $m[2]) + 2 * ($m[0] * $m[2]);
	$volume = $m[0] * $m[1] * $m[2];
	foreach($m as $i => $dim) {
		if ($dim === max($m)) {
			array_splice($m, $i, 1);
			break;
		}
	}
	$ribbon = 2 * $m[0] + 2 * $m[1];
	$smallest = $m[0] * $m[1];
	$whole = $surface + $smallest;
	$sum += $whole;
	$wh_rbn = $ribbon + $volume;
	$r_sum += $wh_rbn;
	echo "$l >>> $ribbon + $volume = $wh_rbn >>>  >>> $r_sum\n";

}

