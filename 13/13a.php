#!/usr/bin/php
<?php
(isset($argv[1]) && is_readable($argv[1])) || die("error reading input file\n");

$file = file($argv[1], FILE_IGNORE_NEW_LINES);

$people = array();
$relations = array();
foreach ($file as $line) {
	$m = array();
	if(!preg_match('/^(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+)\.$/', $line, $m)) {
		die("invalid input\n");
	}
	if(!in_array($m[1], $people)) $people[] = $m[1];
	$relations["$m[1]|$m[4]"] = ($m[2]=='lose'?-1:1) * $m[3];
}

function pc_permute($items, $perms = array( )) {
    if (empty($items)) {
        $return = array($perms);
    }  else {
        $return = array();
        for ($i = count($items) - 1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $perms;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            $return = array_merge($return, pc_permute($newitems, $newperms));
        }
    }
    return $return;
}

$all = pc_permute($people);
$max_rel = 0;
foreach($all as $k => $perm) {
	$perm_rel = 0;
	for($i = 0; $i < count($perm); $i++) {
		$me = $perm[$i];
		$neighbor = $perm[($i+1) % count($perm)];
		$perm_rel += $relations["$me|$neighbor"] + $relations["$neighbor|$me"];
	}
	if($perm_rel > $max_rel) {
		$max_rel = $perm_rel;
	}
}

echo "$max_rel\n";