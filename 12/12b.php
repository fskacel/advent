#!/usr/bin/php
<?php
(isset($argv[1]) && is_readable($argv[1])) || die("error reading input file\n");

$str = file_get_contents($argv[1]);

$obj = json_decode($str);

function children(&$obj, $l = 0) {

	$subsum = 0;
	foreach ($obj as $k => $v) {
		if(is_array($v) || is_object($v)) {
			$subsum += children($v, $l + 1);
		} else {
			if($v === 'red') {
				if(is_object($obj)) return 0;
			} elseif(is_numeric($v)) {
				$subsum += $v;
			}
			echo str_repeat(">", $l) . "$v\t$subsum\n";
		}
		
	}
	return $subsum;
		

}

echo ">>> " . children($obj) . "\n";
