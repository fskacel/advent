<?php
(isset($argv[1]) && is_readable($argv[1])) || die('error reading input file');
$strings = file($argv[1], FILE_IGNORE_NEW_LINES);
$nice = 0;
foreach ($strings as $string) {
	echo "\n$string\t";
	//pair of any 2 letters
	if(!preg_match('/([a-z][a-z])[a-z]*\1/', $string)) {
		continue;
	}

	if(!preg_match('/([a-z])[a-z]\1/', $string)) {
		continue;
	}

	$nice++;

}
echo "nice strings: $nice\n";
