#!/usr/bin/php
<?php

  function combos($arr, $k) {
    if ($k == 0) {
      return array(array());
    }
 
    if (count($arr) == 0) {
      return array();
    }
 
    $head = $arr[0];
 
    $combos = array();
    $subcombos = combos($arr, $k-1);
    foreach ($subcombos as $subcombo) {
      array_unshift($subcombo, $head);
      $combos[] = $subcombo;
    }
    array_shift($arr);
    $combos = array_merge($combos, combos($arr, $k));
    return $combos;
  }
class I {
	public $nam = '';
	public $cap = 0;
	public $dur = 0;
	public $fla = 0;
	public $tex = 0;
	public $cal = 0;

	public function __construct($nam, $cap, $dur, $fla, $tex, $cal) {
		$this->nam = $nam;
		$this->cap = $cap;
		$this->dur = $dur;
		$this->fla = $fla;
		$this->tex = $tex;
		$this->cal = $cal;
	}
}

class C {
	protected $i = array();

	public function add($i, $cnt = 1) {
		var_dump($i); var_dump($cnt);
		if(count($this->i) > 100 - $cnt) return false;
		for($j = 0; $j < $cnt; $j++) {
			$this->i[] = $i;
		}
		die;
		return true;
	}

	public function getScore() {
		$cap = 0; $dur = 0; $fla = 0; $tex = 0;
		foreach($this->i as $i) {
			$cap += $i->cap;
			$dur += $i->dur;
			$fla += $i->fla;
			$tex += $i->tex;
		}
		$cap = $cap < 0 ? 0 : $cap;
		$dur = $dur < 0 ? 0 : $dur;
		$fla = $fla < 0 ? 0 : $fla;
		$tex = $tex < 0 ? 0 : $tex;
		return $cap * $dur * $fla * $tex;
	}
}

(isset($argv[1]) && is_readable($argv[1])) || die("error reading input file\n");

$c = new C();

$is = array();

$specs = file($argv[1], FILE_IGNORE_NEW_LINES);
foreach($specs as $spec) {
	$m = array();
	if(!preg_match('/^(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)$/', $spec, $m)) {
		die("invalid input\n");
	}
	$is[] = new I($m[1], $m[2], $m[3], $m[4], $m[5], $m[6]);
}

$ms = 0;

function re($lvl = 0) {
	if($lvl > 3) return; else re($lvl + 1);
	echo $lvl;
}

re();

echo "$ms\n";
