<?php
(isset($argv[1]) && is_readable($argv[1])) || die ("ERROR opening input file\n\n");
$f = file($argv[1]);
$sum = 0;
foreach ($f as $l) {
	$m = [];
	preg_match('/(\d+)x(\d+)x(\d+)/', $l, $m);
	array_splice($m, 0, 1);
	$surface = 2 * ($m[0] * $m[1]) + 2 * ($m[1] * $m[2]) + 2 * ($m[0] * $m[2]);
	foreach($m as $i => $dim) {
		if ($dim === max($m)) {
			array_splice($m, $i, 1);
			break;
		}
	}
	$smallest = $m[0] * $m[1];
	$whole = $surface + $smallest;
	$sum += $whole;
	echo "$l >>> $surface + $smallest = $whole >>> $sum\n";

}

