<?php
  function combos($arr, $k) {
    if ($k == 0) {
      return array(array());
    }
 
    if (count($arr) == 0) {
      return array();
    }
 
    $head = $arr[0];
 
    $combos = array();
    $subcombos = combos($arr, $k-1);
    foreach ($subcombos as $subcombo) {
      array_unshift($subcombo, $head);
      $combos[] = $subcombo;
    }
    array_shift($arr);
    $combos = array_merge($combos, combos($arr, $k));
    return $combos;
  }
 
  $arr = array("iced", "jam", "plain");
  $result = combos($arr, 10);
  foreach($result as $combo) {
    echo implode(' ', $combo), "\n";
  }
?>
