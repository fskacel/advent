<?php
(isset($argv[1]) && is_numeric($argv[1])) || die('invalid input' . "\n");
(isset($argv[2]) && is_numeric($argv[2])) || die('invalid input' . "\n");

$inp = $argv[1];
$ic = $argv[2];

for ($i = 0; $i < $ic; $i++) {
	$m = array();
	preg_match_all('/(\d)\1*/', $inp, $m);
	$o = '';
	foreach($m[0] as $n) {

		$o .= strlen($n) . substr($n, 0, 1);
	}
	$inp = $o;
}
echo "length after $ic iterations: ".strlen($o)."\n";
