<?php
(isset($argv[1]) && preg_match('/^[a-z]{8}$/', $argv[1])) || die("invalid input\n");
$r = isset($argv[2])?$argv[2]:1;
$inp = $argv[1];

function inc($inp) {
	$carry = TRUE;
	for($i = strlen($inp) - 1; $i >= 0; $i--) {
		if($carry) {
			if($inp[$i] === 'z') {
				$inp[$i] = 'a';
			} else {
				$inp[$i] = chr(ord($inp[$i]) + 1);
				$carry = FALSE;
				return $inp;
			}
		}
	}
	return $inp;
}

function test1($inp) {
	if (strlen($inp) < 3) return FALSE;
	for($i = 0; $i < strlen($inp) - 2; $i++) {
		if($inp[$i] == chr(ord($inp[$i + 1]) - 1) && $inp[$i + 1] == chr(ord($inp[$i + 2]) - 1)) {
			return TRUE;
		}
	}
	return FALSE;
}

function test2($inp) {
	return !preg_match('/i|o|l/', $inp);
}

function test3($inp) {
	$m = array();
	if(preg_match_all('/(\w)\1/', $inp, $m) < 2) return FALSE;
	if(count(array_unique($m[0])) < 2) return FALSE;
	return TRUE;

}

for ($i = 0; $i < $r; $i++) {
	do {
		$inp = inc($inp);
	} while (!test3($inp) || !test2($inp) || !test1($inp));
	echo "$i >>> $inp\n";
}
