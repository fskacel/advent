<?php
	isset($argv[1]) || die('missing parameter');

	define('LIMIT', 1000000000);

	$key = $argv[1];

	for ($i = 0; $i < LIMIT; $i++) {
		if(strpos(md5($key . $i), '00000') === 0) {
			echo "$i => " . md5($key . $i);
			die("\n");
		}
	}

	echo "\n";
