<?php
(isset($argv[1]) && is_readable($argv[1])) || die('error reading input file');

$lines = file($argv[1], FILE_IGNORE_NEW_LINES);
$ds = 0;
foreach($lines as $line) {
	$lc = strlen($line);
	$nline = preg_replace_callback(
		'/\\\x[a-f0-9]{2}/', 
		function($m) {
			return chr(hexdec($m[0]));
		}, 
		$line
	);
	$nline = preg_replace_callback(
		'/\\\([\\\"])/',
		function($m) {
			return $m[1];
		},
		$nline
	);

	$nline =  substr($nline, 1, strlen($nline) - 2);

	$lm = strlen($nline);
	echo "$lc - $lm = " . ($lc - $lm) . "\t$line\t|\t$nline\n";
	$ds += $lc - $lm;
}
echo "==========\n$ds\n";
