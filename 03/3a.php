<?php
$x = 0;
$y = 0;

(isset($argv[1]) && is_readable($argv[1])) || die('error opening file');

$file = file_get_contents($argv[1]);

$map = ['0|0'];
for ($i = 0; $i < strlen($file) - 1; $i++) {
	switch($file[$i]) {
		case '^': $y++; break;
		case 'v': $y--; break;
		case '>': $x++; break;
		case '<': $x--; break;
		default: die('invalid input');
	}
		$coords = "$x|$y";

	echo $coords;

	if (!in_array($coords, $map, TRUE)) {
		$map[] = $coords;
		echo ' *';
	}

	echo "\n";

}

echo 'visited houses:' .  count($map) . "\n\n";
