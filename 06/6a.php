<?php
(isset($argv[1]) && is_readable($argv[1])) || die('error reading input file');
$ins = file($argv[1], FILE_IGNORE_NEW_LINES);
define('SIZE', 1000);
$map = [];
for($x = 0; $x < SIZE; $x++) {
	for ($y = 0; $y < SIZE; $y++) {
		$map[$x][$y] = 0;
	}
}
foreach($ins as $in) {
	$m = [];
	preg_match('/(\d+)[^\d]*(\d+)[^\d]*(\d+)[^\d]*(\d+)/', $in, $m);
	for ($x = $m[1]; $x <= $m[3]; $x++) {
		for ($y = $m[2]; $y <= $m[4]; $y++) {
			if(preg_match('/^toggle/', $in)) {
				$map[$x][$y] = !$map[$x][$y];
			} elseif(preg_match('/^turn off/', $in)) {
				$map[$x][$y] = 0;
			} elseif(preg_match('/^turn on/', $in)) {
				$map[$x][$y] = 1;
			} else {
				die("invalid input\n");
			}
			
		}
	}
}
$lit = 0;
for($x = 0; $x < SIZE; $x++) {
	for ($y = 0; $y < SIZE; $y++) {
		$lit += $map[$x][$y];
	}
}

echo "lit $lit\n";
