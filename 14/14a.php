#!/usr/bin/php
<?php

class RD {
	public $name = '';
	public $speed = 0;
	public $fly_time = 0;
	public $rest_time = 0;
	protected $distance = 0;

	public function __construct($name, $speed, $fly_time, $rest_time) {
		$this->name = $name;
		$this->speed = $speed;
		$this->fly_time = $fly_time;
		$this->rest_time = $rest_time;
	}

	public function fly($duration) {
		for($t = 0; $t < $duration; $t++) {
			$t1 = $t % ($this->fly_time + $this->rest_time);
			if($t1 < $this->fly_time) {
				$this->distance += $this->speed;
			}
		}
		return $this->distance;
	}
}

(isset($argv[1]) && is_readable($argv[1])) || die("error reading input file\n");
(isset($argv[2]) && is_numeric($argv[2])) || die("invalid duration specification\n");

$file = file($argv[1], FILE_IGNORE_NEW_LINES);
$rds = array();
$dur = $argv[2];
foreach($file as $rd_spec) {
	$m = array();
	if(!preg_match('/^(\w+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds\.$/', $rd_spec, $m)) {
		die ("invalid input\n");
	}
	$rds[] = new RD($m[1], $m[2], $m[3], $m[4]);
}

foreach($rds as $rd) {
	echo $rd->name.": ". $rd->fly($dur) . "\n";
}
