<?php
(isset($argv[1]) && is_readable($argv[1])) || die('error reading input file');
$strings = file($argv[1], FILE_IGNORE_NEW_LINES);
$nice = 0;
foreach ($strings as $string) {
	echo "\n$string\t";
	// vowels testing
	$vowels = 0;
	for ($i = 0; $i < strlen($string); $i++) {
		if ($string[$i] == 'a' || $string[$i] == 'e' || $string[$i] == 'i' || $string[$i] == 'o' || $string[$i] == 'u') {
			$vowels++;
			if ($vowels >= 3) break;
		}
	}
	echo "$vowels\t";
	if ($vowels < 3) continue;
	// letter twice in a row
	if (!preg_match('/([a-z])\1/', $string)) {
		continue;
	}
	// forbidden strings
	if (preg_match('/ab|cd|pq|xy/', $string)) {
		continue;
	}
	$nice++;

}
echo "nice strings: $nice\n";
