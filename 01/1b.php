<?php
isset($argv[1]) || die("ERROR: no input\n\n");

$input = $argv[1];

$floor = 0;

for ($i = 0; $i < strlen($input); $i++) {
	
	switch ($input[$i]) {
		case '(':
			$floor++;
			break;
		case ')':
			$floor--;
			break;
		default:
			die("ERROR: invalid input\n\n");
	}
	echo "$i >> " . $input[$i] . "\t$floor\n";

	if ($floor == -1) {
		die('finish\n');
	}
}
