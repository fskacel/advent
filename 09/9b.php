<?php
function pc_permute($items, $perms = array( )) {
    if (empty($items)) { 
		$return = array($perms);
	}  else {
		$return = array();
		for ($i = count($items) - 1; $i >= 0; --$i) {
			$newitems = $items;
			$newperms = $perms;
			list($foo) = array_splice($newitems, $i, 1);
			array_unshift($newperms, $foo);
			$return = array_merge($return, pc_permute($newitems, $newperms));
		}
	}
	return $return;
}

(isset($argv[1]) && is_readable($argv[1])) || die("error reading input file $argv[1]\n");
$dstncs = file($argv[1], FILE_IGNORE_NEW_LINES);
$lcs = array();
foreach($dstncs as $d) {
	$m = array();
	if(!preg_match('/^([a-z]+) to ([a-z]+) = \d+$/i', $d, $m)) {
		die("invalid input\n");
	}
	if(!in_array($m[1], $lcs)) $lcs[] = $m[1];
	if(!in_array($m[2], $lcs)) $lcs[] = $m[2];
}

$rts = pc_permute($lcs);

$dm = 0;
foreach($rts as $rt) {
	$dr = 0;
	for($i = 0; $i < count($rt) - 1; $i++) {
//		echo "$rt[$i] >> ";
		foreach($dstncs as $d) {
			$m = array();
			if(preg_match('/(' . $rt[$i] . ' to ' . $rt[$i + 1] . '|' . $rt[$i + 1] . ' to ' . $rt[$i] . ') = (\d+)/', $d, $m)) {
				$dr += (int) $m[2];
			}
		}
	}
//	echo "$rt[$i] >>> $dr\n";
	$dm = ($dm < $dr) ? $dr : $dm;
}

echo "maximal distance: $dm\n";

