<?php
(isset($argv[1]) && is_readable($argv[1])) || die('error reading input file');

$lines = file($argv[1], FILE_IGNORE_NEW_LINES);
$ds = 0;
foreach($lines as $line) {
	$lc = strlen($line);
	$nline = '"' . preg_replace_callback(
		'/\\\|\\"/', 
		function($m) {
			return '\\' . $m[0];
		}, 
		$line
	) . '"';
	$lcc = strlen($nline);
	echo "$line\t|\t$nline\n";
	$ds += $lcc - $lc;
}
echo "==========\n$ds\n";
