<?php
(isset($argv[1]) && is_readable($argv[1])) || die("error reading file\n");
$ins = file($argv[1], FILE_IGNORE_NEW_LINES);
$continue = TRUE;
for ($i = 0; $continue; $i++) {
	$continue = FALSE;
	foreach($ins as $k => $in) {
		$before = substr($in, 0, strpos($in, ' -> '));
		$after = substr($in, strpos($in, ' -> ') + 4);
		if(!preg_match('/[a-z]/', $before)) {
			array_splice($ins, $k, 1);
			break;
		}
	}
	foreach($ins as $k => $in) {
		if (preg_match('/\b'.$after.'\b/', $ins[$k])) {
			$ins[$k] = preg_replace('/\b' . $after . '\b/', '(' . $before . ')', $ins[$k]);
			$continue = TRUE;
		}
	}
	echo count($ins) . "\n";
}
echo "Result after $i rounds:\n";
foreach($ins as $k => $in) {
	$before = substr($in, 0, strpos($in, ' -> '));
	$after = substr($in, strpos($in, ' -> ') + 4);
	echo "$after = $before\n";
}
